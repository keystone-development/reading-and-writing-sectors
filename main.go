package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"flag"
    "fmt"
	"hash"	
	"io"
	"io/ioutil"
	"os"
	"golang.org/x/sys/windows"
)

type arguments struct {
	// 0: Read DiskSector
	// 1: Write DiskSector
	// 2: Encrypt File
	// 3: Decrypt File
	// 4: Hash String 
	// 5: Hash File
	mode		int

	// DiskSector Read & Write Fields
	diskindex 	int
	offset      int64
	readBytes	int	
	buffer		string

	// File Crypto Encrypt & Decrypt Fields
	inpath		string
	pwd			string

	// Hash Field
	input		string
}


func getArgs() arguments {
	args := arguments{}
	
	flag.IntVar(&args.mode,			"mode",			0,		"Read or Write")
	flag.IntVar(&args.diskindex,	"diskidx",		0,		"Physical Disk Index")
	flag.Int64Var(&args.offset,		"offset",		0,		"Offset to read or write")
	flag.IntVar(&args.readBytes,	"readbyte",		0,		"Read Bytes")
	flag.StringVar(&args.buffer,	"buffer",		"",		"Buffer String")
	flag.StringVar(&args.inpath,	"inputpath",	"",		"Input Filepath")
	flag.StringVar(&args.pwd, 		"pwd",			"",		"password for crypt")
	flag.StringVar(&args.input, 	"inputhash",	"",		"Input String or File path")
	flag.Parse()

	return args
}

// DiskSector Read & Write
func readSector(diskidx int, offset int64, readBytes int) (string)  {

	diskpath := fmt.Sprintf("\\\\.\\PHYSICALDRIVE%d", diskidx)

	var fd windows.Handle
	var err error
	mode := uint32(windows.FILE_SHARE_READ | windows.FILE_SHARE_WRITE)
	flags := uint32(windows.FILE_ATTRIBUTE_READONLY)
	
	fd, err = windows.CreateFile(windows.StringToUTF16Ptr(diskpath), windows.GENERIC_READ, mode, nil, windows.OPEN_EXISTING, flags, 0)

    if err != nil {
        fmt.Print("Open Failed: %v\n", err.Error())
        return ""
    } else {
		fmt.Printf("Disk Opened: Handle [%v]\n", fd)
	}
	
	buffer := make([]byte, 512)
	
	var bytesReturned uint32
	var startOffset int64
	startOffset = (offset / 512) * 512;
	
	windows.Seek(fd, startOffset, windows.FILE_BEGIN);	
	err = windows.ReadFile(fd, buffer, &bytesReturned, nil)
	if err != nil {
		return ""
	}	

	windows.CloseHandle(fd);

	var fromString int
	fromString = (int)(offset - startOffset)	
	readStr := string(buffer[fromString:readBytes])

	return readStr;
}

func writeSector(diskidx int, offset int64, buffer string) (int){

	diskpath := fmt.Sprintf("\\\\.\\PHYSICALDRIVE%d", diskidx)

	var fd windows.Handle
	var err error
	mode := uint32(windows.FILE_SHARE_READ | windows.FILE_SHARE_WRITE)
	flags := uint32(windows.FILE_ATTRIBUTE_READONLY)
	
	fd, err = windows.CreateFile(windows.StringToUTF16Ptr(diskpath), windows.GENERIC_READ | windows.GENERIC_WRITE, mode, nil, windows.OPEN_EXISTING, flags, 0)

    if err != nil {
        fmt.Print("Open Failed: %v\n", err.Error())
        return 0
    } else {
		fmt.Printf("Disk Opened: Handle [%v]\n", fd)
	}
	
	var bytesReturned uint32
	var bytesWritten uint32
	var startOffset int64
	startOffset = (offset / 512) * 512;
	
	diskbuffer := make([]byte, 512)
	var bufOffset int
	bufOffset = (int)(offset - startOffset)	
	
	windows.Seek(fd, startOffset, windows.FILE_BEGIN);	
	err = windows.ReadFile(fd, diskbuffer, &bytesReturned, nil)
	
	windows.Seek(fd, startOffset, windows.FILE_BEGIN);

	bufLen := len(buffer)
	copy(diskbuffer[bufOffset:bufLen], buffer)
	
	err = windows.WriteFile(fd, diskbuffer, &bytesWritten, nil)
	if err != nil {
		fmt.Print("WriteFile Failed: %v\n", err.Error())
		return 0
	}	

	windows.CloseHandle(fd);

	return 1;
}

// File Crypto Encrypt & Decrypt
func CreateHash(byteStr []byte) []byte {
	var hashVal hash.Hash
	hashVal = sha256.New()
	hashVal.Write(byteStr)
	var bytes []byte
	bytes = hashVal.Sum(nil)
	return bytes
}

func EncryptBytes(data []byte, passphrase []byte) ([]byte, error) {
	key := []byte(CreateHash(passphrase))
	key = key[0:16]
		
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	ciphertext := make([]byte, aes.BlockSize+len(string(data)))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], data)
	
	return []byte(base64.StdEncoding.EncodeToString(ciphertext)), nil
}

func DecryptBytes(data []byte, passphrase []byte) ([]byte, error) {
	key := []byte(CreateHash(passphrase))
	key = key[0:16] 

	text, _ := base64.StdEncoding.DecodeString(string(data))
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	if len(text) < aes.BlockSize {
		return nil, err
	}
	iv := text[:aes.BlockSize]
	text = text[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(text, text)

	return text, nil
}

func CreateHashFromString(orgStr string) []byte {
	
	return CreateHash([]byte(orgStr))
}

func CreateHashFromFile(filepath string) []byte {

	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		fmt.Println("File reading error", err)
		return nil
	}

	return CreateHash(data)
}

func main() {
	
	argcount := len(os.Args)
	if (argcount != 5 && argcount != 7 && argcount != 9) {
		fmt.Println("[usage] updiskrw.exe -mode [0(read)]	-diskidx [index] -offset [offset] -readbyte [readcount]");
		fmt.Println("[usage] updiskrw.exe -mode [1(write)]	-diskidx [index] -offset [offset] -buffer [write string]");
		fmt.Println("[usage] updiskrw.exe -mode [2(encrypt)] -inputpath [input filepath] -pwd [password] ");
		fmt.Println("[usage] updiskrw.exe -mode [3(decrypt)] -inputpath [input filepath] -pwd [password] ");
		fmt.Println("[usage] updiskrw.exe -mode [4(string)]  -inputhash [string]")
		fmt.Println("[usage] updiskrw.exe -mode [5(file)]    -inputhash [filepath]")
		return;
	}

	args := getArgs()

	// Test code to read sector
	if (args.mode == 0) {
		strKey := readSector(args.diskindex, args.offset, args.readBytes)	
		fmt.Println("ReadSector OK!\nlickey", strKey)
		return
	}
	
	// Test code to write sector
	if (args.mode == 1) {
		strKey := args.buffer;
		writeSector(args.diskindex, args.offset, strKey);
		fmt.Println("WriteSecter OK!")
		return
	}

		// Encrypt File
	if (args.mode == 2) {
		strInFile := args.inpath;
		strOutFile := args.inpath + ".enc";
		
		data, err := ioutil.ReadFile(strInFile)
		if err != nil {
			fmt.Println("File reading error", err)
			return
		}
		
		password := []byte(args.pwd)
		readBytes, err2 := EncryptBytes(data, password)
		if err2 != nil {
			fmt.Println("EncryptBytes error", err2)
			return
		}
		
		err = ioutil.WriteFile(strOutFile, readBytes, 0644)
		if err != nil {
			fmt.Println("File writing error", err)
			return
		}
	
		fmt.Println("Encrypt FileOK!")
		return
	}
	
	// Decrypt File
	if (args.mode == 3) {
		strInFile := args.inpath;
		strOutFile := args.inpath + ".dec";
		
		data, err := ioutil.ReadFile(strInFile)
		if err != nil {
			fmt.Println("File reading error", err)
			return
		}		
	
		password := []byte(args.pwd)
		readBytes, err2 := DecryptBytes(data, password)	
		if err2 != nil {
			fmt.Println("DecryptBytes error", err2)
			return
		}
		
		err = ioutil.WriteFile(strOutFile, readBytes, 0644)
		if err != nil {
			fmt.Println("File writing error", err)
			return
		}		

		fmt.Println("Decrypt FileOK!")
		return
	}

	// Hash String 
	if (args.mode == 4) {
		data := CreateHashFromString(args.input)
		fmt.Println(data)
	}

	if (args.mode == 5) {
		data := CreateHashFromFile(args.input)
		fmt.Println(data)
	}
}